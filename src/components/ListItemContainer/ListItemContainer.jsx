import { element } from 'prop-types';
import React, {useState, useEffect } from 'react';
import { ListItemContext } from '../ListItemContext/ListItemContext';

const ListItemContainer = (props) => {

    let [cards, setItems] = useState([]);
    let [cart, setCart] = useState([]);
    const [show, showModal] = useState(false);
    const [modal, modalForm] = useState();
    let [favorite, setFavorite] = useState([]);  


    const openModal = (id) => {
      showModal(true);
      const dataForModal = cart.find(item => item.id === +id); 
      modalForm(dataForModal);  
    }

    const addToCart = (id) => {
        showModal(true);
        let dataForModal = cards.find(item => item.id === +id); 
        modalForm(dataForModal = {... dataForModal, idForDelete: Date.now()});
        cart.push(dataForModal);
        localStorage.setItem('cart', JSON.stringify(cart));
      }
    
        
    const closeModal = () => {
      showModal(false)  
    }
    

    const clearCart = () => {
        localStorage.removeItem('cart');
        setCart([]);
      }


      const handleDelete = (id) => {
        const newCart = cart.filter((el) => {
          if (el.idForDelete == id) {
            return false
          } else {
            return true
          }
        })
        setCart(newCart)
        localStorage.setItem('cart', JSON.stringify(newCart));
        closeModal();
      }
    
    useEffect(() => {
        fetch('./data/json-data.json')
        .then(response => response.json())
        .then(item => {
          setItems(item);
        if (localStorage.getItem("cart") !== null) {setCart(JSON.parse(localStorage["cart"]))};
        })
    }, []);

    useEffect(() => {
      if (localStorage.getItem("favorites") !== null) {setFavorite(JSON.parse(localStorage["favorites"]))
};
}, [])


const favoritesListId = favorite;
const productsList = cards.map((element) => {
  favoritesListId.includes(element)
    ? (element.isFavorites = true)
    : (element.isFavorites = false);
  return element;
});

//cards = productsList

console.log(cards);

  // const temp = favorite;
  // let newCards = cards.map((el) => {
  //   temp.includes(el)
  //      ? (el.isFavorites = true)
  //      : (el.isFavorites = false)
  //    return element;
  //  });
  //    cards = newCards;


  // let newCards = cards.map((element) => {
  //   favorite.includes(element)
  //      ? (element.isFavorites = "--js-active")
  //      : (element.isFavorites = "--js-non-active")
  //    return element;
  //  });
  //    cards = newCards;
  //    console.log(favorite);


      // const newCards = cards.map((element) => {
      //   favorite.includes(element)
      //      ? (element.isFavorites = "--js-active")
      //      : (element.isFavorites = "--js-non-active")
      //    return element;
      //  });
      //  cards = newCards;

       const addToFavorite = (el) => {
        if (favorite.includes(el)) {
          cards.map(function(item) {
            if(item.id === el.id) {
              item.isFavorites = false
            };
            });
            const newFavorites = favorite.filter((item) => item.id !== el.id);
            setFavorite(newFavorites);
        } else {
          cards.map(function(item) {
            if(item.id == el.id) {
              item.isFavorites = true
            };
            });
            setFavorite(arr => [...arr, el])
        };
    }

    useEffect(() => {
      localStorage.setItem("favorites", JSON.stringify(favorite));
  }, [favorite])




    // const addToFavorite = (id) => {
    //     favorite = favorite.map((id) => id);
    //     if (favorite.includes(id)) { 
    //       // cards.map(function(el) {
    //       //   if(el.id === id) {
    //       //     el.isFavorites = false
    //       //   };
    //       // });
    //       favorite = favorite.filter((item) => item.id !== id);
    //       localStorage.setItem("favorites", JSON.stringify(favorite));
    //     } else {
    //       cards.map(function(el) {
    //         if(el.id === id) {
    //           el.isFavorites = true
    //           favorite.push(el.id)
    //           localStorage.setItem("favorites", JSON.stringify(favorite));
    //         };
    //       });
    //     }
    //     setFavorite(favorite)
    // }
    const { children } = props;


    return (
        <ListItemContext.Provider value={{
            cards,
            cart: cart,
            setCart: setCart,
            clearCart: clearCart,
            onDelete: handleDelete,
            addToCart: addToCart,
            closeModal: closeModal,
            addToFavorite: addToFavorite,
            openModal: openModal,
            show,
            modal,
            favorite,
        }}>
        {children}
        </ListItemContext.Provider>
      );
    
};

export default ListItemContainer;