import React from "react";
import './Modal.scss';
import PropTypes from 'prop-types';


const Modal = (props) => {
  return (
    <div>
      <>
             <div className="modal" onClick={props.onClick}>
             <div className="modal-content" style={{backgroundColor: props.backgroundColor}} onClick={ e => e.stopPropagation()}>
                 <div className="modal-content__header">{props.header}
                 {props.closeBtn  && <span className="modal-content__close-btn-cross" onClick={props.onClick}></span>}
                 </div>    
                 <div className="modal-cart-container">
                   <div className="cartImage-container" >
                     <img className="cartImage" src={props.imgRoute} alt="" />
                   </div>                   
                   <p>{props.name}</p>
                   <p>Цена: {props.price} ₴</p>
                 </div>   
                 {props.actions}
             </div>
           </div>
          </>
      
    </div>
  );
};

export default Modal;

Modal.propTypes = {
  price: PropTypes.number,
  onClick: PropTypes.func,
  imgRoute: PropTypes.string,
  name: PropTypes.string,
};

Modal.defaultProps = {
  header:'Товар добавлен в корзину',
  closeBtn:'true',
  backgroundColor:'rgba(9, 82, 9, 0.7)'
};


