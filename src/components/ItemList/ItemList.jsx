import React, {useEffect, useContext } from 'react';
import Item from '../Item/Item';
import Button from '../Button/Button';
import Modal from '../Modal/Modal';
import Header from '../Header/Header';
import { ListItemContext } from '../ListItemContext/ListItemContext';
import { useNavigate } from "react-router-dom";


const ItemList = () => {
    const navigate = useNavigate();

const {
    onCollectionFirstRender,
    cards,
    cart,
    clearCart,
    addToCart,
    closeModal,
    addToFavorite,
    show,
    modal,
    favorite,
} = useContext(ListItemContext)

  

  const moveTo = (e) => {
    const route = "/cart";
     navigate(route)
     closeModal()
 }


  return (
    <div>
      < Header
      itemCount={cart.length}
      favCount={favorite.length}
               actions = {
                 <>
                 <Button 
                 btnClassName="button"
                 onClick={clearCart}
                 className="button"
                 text="Очистить корзину"/>
                 </>
               }
      />
       <ul className="item-list">
           {cards.map(el => (
             < Item 
             id={el.id}
            classNameIcon={"favor-icon"}
            isFavorites={el.isFavorites}
             onClick={() => addToFavorite(el)}
             modalId={el.id}
             key={el.id}
             imgRoute={el.imgRoute}
             name={el.name}
             price={el.price}   
             actions = {
               <>
               <Button 
               modalId={el.id}
               btnClassName="button apply"
               onClick={() => addToCart(el.id)}
               className="button"
               text="Добавить в корзину"/>
               </>
             }
             />            
           ))}
         </ul>   
         {
         show && <Modal
         onClick={closeModal}
           id={modal.id}  
           imgRoute={modal.imgRoute}
           name={modal.name}
           price={modal.price}
           actions = {
            <>
            <Button 
            btnClassName="button apply"
            onClick={moveTo}
            className="button"
            text="Перейти в корзину"
            />
            <Button 
            btnClassName="button confirm"
            onClick={closeModal}
            className="button"
            text="Продолжить покупки"
            />
            </>
          }
         />        
         }   
    </div>
  );
};

export default ItemList;